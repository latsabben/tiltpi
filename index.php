<?php 
include("inc/header.php"); 
if(!empty($_GET['batch'])) {
   $batch=$_GET['batch'];
   $beerdata = $beers->$batch;
   if(file_exists("DATA/batches/$batch")) {
	$rawdata = file("DATA/batches/$batch");
	$startData = explode(",", $rawdata[0]);
	$start     = gmdate("j M Y @ H:i",$startData[0]);
	$i=$t=$OG=$FG=0; // initialize
	while($i <=6) {
        	$foo = explode(",", $rawdata[$i]);
		$OG_holder = $foo[4];
        	$OG+=$foo[4];
	        $i++;
		}
	
	// Flip and reverse
	$rawdata = array_reverse($rawdata);
	$endData = explode(",", $rawdata[0]);
        $end     = gmdate("j M Y @ H:i",$endData[0]);
	$i	 = 0;
	while($i <=6) {
        	$foo = explode(",", $rawdata[$i]);
        	$FG_holder=$foo[4];
		$FG+=$foo[4];
	        $i++;
		}
	
		// Getting some data points
        $days = round(($endData[0]-$startData[0])/60/60/24, 1);
        
	// Don't use round if not atleast 6 readings
	if(count($rawdata) < 7) {
		$too_early = explode(",", $rawdata[0]);
		$FG = $too_early[4];
		$OG = $too_early[4];
		}
	else {
		$OG = round($OG/7,0); 
		$FG = round($FG/7,0); 
	}
        $ABV = round((($OG/1000)-($FG/1000))*131.25,1);

	// Temp average
	$temp_array = array();
	$temp_avg = $temp_max = 0;
	$temp_min=99999;
	foreach($rawdata as $dataline){
		 $data = explode(",", $dataline);
		 array_push($temp_array,$data[3]);
		 if($data[3]>$temp_max) { $temp_max=$data[3]; }
		 if($data[3]<$temp_min) { $temp_min=$data[3]; }
		 $temp_avg +=$data[3];
		}
	$temp_avg = round((($temp_avg/count($temp_array))-32)/1.8,2);
	$temp_min = round(($temp_min-32)/1.8,2);
	$temp_max = round(($temp_max-32)/1.8,2);

	// chunk to get hours average
	$chunkdata = array_chunk($rawdata, 12); // Hour average
	$smoothdata = [];
        foreach($chunkdata as $chunk) {
		$SG = 0;
		$timestamp = 0;
		foreach($chunk as $d) {
			$data = explode(",", $d);
			$timestamp+=$data[0];
			$SG = $SG + intval($data[4]);
			}
		$SG = round($SG/12);
		$timestamp = $timestamp/12;
		$smoothdata[$timestamp] = $SG;
	}
	 // Stable or not?
        $min = $max = intval($FG);

	foreach ($smoothdata as $timestamp => $SG){
                if(!isset($stabil)) {
                        if($SG > $max) { $max = $SG; }
                        if($SG < $min) { $min = $SG; }
                        if($max-2 > $min) {
                                $stabil = $timestamp;
                        	}
                	} 
		else { 	break;}
                }
        $stabil_days = round(($endData[0]-$stabil)/60/60/24, 1);
	// Out of bound check, reset to 0
	if($stabil_days > 10000) { $stabil_days = 0; }

?>
<!-- Java inline script for passthrough var and highcharts -->
<script type="text/javascript">
                var beerData = {};
                beerData.targetFG = '<?php echo $beerdata->fg;?>';
                beerData.batchnr  = '<?php echo $beerdata->batchnr;?>';
                beerData.name     = '<?php echo $beerdata->name;?>';
		</script>

<br>
<script src="js/highchart.js"></script>
<div id="chart"></div>
<br>
<table class="table text-left table">
<!-- <thead>
                <tr>
                        <th class="text-center" colspan="2">&nbsp;</th>
                        <th class="text-center" colspan="2">OG/FG/ABV</th>
                        <th class="text-center" colspan="2">Date / Time</th>
                        </tr>
                </thead> -->

          <tbody> 
	    <tr>
		<td class="table-title"><b>Batch</b></td><td><?php echo $beerdata->batchnr; ?></td>
		<td class="table-title">OG</td><td><?php echo $OG;?></td>
		<td class="table-title hidden-xs">Days</td><td class="hidden-xs"><?php echo "$days days of fermentation";?></td>
                <td class="table-title">Avg </td><td><?php echo  $temp_avg; ?> C&deg</td>
		</tr>
           <tr>
<tr>
                <td class="table-title">Name</td><td><?php echo $beerdata->name; ?></td>
                <td class="table-title">SG</td><td><?php echo "$FG ($beerdata->fg)";?></td>
		<td class="table-title hidden-xs">Start</td><td class="hidden-xs"><?php echo $start;?></td>
                <td class="table-title hidden-xs">Min </td><td class="hidden-xs"><?php echo "$temp_min";?> C&deg</td>
		</tr>
           <tr>
<tr>
<tr>
                <td class="table-title">Type</td><td><?php echo $beerdata->type; ?></td>
                <td class="table-title">ABV</td><td><?php echo $ABV;?>% </td> 
		<td class="table-title hidden-xs">End</td><td class="hidden-xs"><?php echo $end;?></td>
		<td class="table-title hidden-xs">Max </td><td class="hidden-xs"><?php echo "$temp_max";?> C&deg</td>
                
</tr>

 <tr>

<tr>

	</tbody>
</table>
<h4 class="text-center">SG stabil for <?php echo $stabil_days;?> days</h4>
<?php
}
else {
	echo "<h2>ERROR: Data fermentation found</h2>";
	echo "Have you started the fermentation process?";
}

	}
else {
	$beers = json_decode(file_get_contents("DATA/batchdb"));

   	// Getting tilts db
        $tiltsdb = json_decode(file_get_contents("DATA/tiltsdb"));
        ?>

        <h2>Select batch<span class="hidden-xs"> from from list</span></h2>
	<div style="text-align: left">
	<div class="table-responsive">
	<table class="table">
	  <thead>
		<tr>
			<th>Batch</th>
			<th>Name</th>
			<th>Brewing</th>
			<th class="hidden-xs">Type</th>
			<th class="text-right"><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">Add <span class="glyphicon glyphicon-plus"></span></button></th>
		</tr>
	  </thead>
  	  <tbody>

	<?php
	foreach($beers as $beer) { 
		if(isset($beer->tilt)) {
			$uuid = $beer->tilt;
			$color = $tiltsdb->$uuid->color;
			}
			?>
		<tr>
			<td><?php echo $beer->batchnr; 	?></td>
			<td>
				<a href="<?php echo "?batch=".$beer->batchnr;?>">
				<?php echo $beer->name; ?>
				</a></td>
			<td>
				<?php if(isset($beer->tilt)) { echo '<span class="glyphicon glyphicon-tag '.$color.'"></span>'; } ?>
				</td>
			<td class="hidden-xs"><?php echo $beer->type;	?></td>
			<td class="text-right">
				<a href="#" class="open-startmodal" data-id="<?php echo $beer->batchnr;?>" data-toggle="modal" >
					<span class="glyphicon glyphicon-play"></span></a> &nbsp;
				<a href="functions.php?stop&batch=<?php echo $beer->batchnr;?>">
					<span class="glyphicon glyphicon-stop"></span></a> &nbsp;
				<!-- No edit function yet ... <span class="glyphicon glyphicon-edit"></span> &nbsp; -->
				<a href="functions.php?del&batch=<?php echo $beer->batchnr;?>" onclick="return confirm('Delete Batch?')">
					<span class="glyphicon glyphicon glyphicon-trash"> </span> </a>
			</td>
		</tr>
	<?php } // end foreach ?>
	  </tbody>
	</table>
	</div> <!--- end table-responsive --->
	</div>
<?php
}
include("inc/footer.php");
?>

