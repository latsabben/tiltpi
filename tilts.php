<?php include("inc/header.php");?>

<h1> Tilts </H1>
<div style="text-align: left">
        <div class="table-responsive">
        <table class="table">
          <thead>
                <tr>
                        <th>Color</th>
                        <th class="hidden-xs">MAC</th>
                        <th class="hidden-xs">UUID</th>
			<th>IN USE</th>
                        <th>&nbsp;</th>
                        </tr>
                </thead>
        <tbody>

<?php 
foreach($tiltsdb as $tilt) {
?>
	 	<tr>
                        <td><?php echo $tilt->color; ?></td>
                        <td class="hidden-xs"><?php echo $tilt->MAC;  ?></td>
			<td class="hidden-xs"><?php echo $tilt->UUID; ?></td>
			<td><?php if(!empty($tilt->BATCH)) { echo "Batch:<a href=\"./?batch=$tilt->BATCH\">$tilt->BATCH</a>"; }?></td>
                	</tr>
<?php } ?>

            </tbody>
        </table>



<?php include("inc/footer.php"); ?>
