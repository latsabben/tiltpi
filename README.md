# TILTpi#

A small project to replace [Tilt™ Floating Wireless Hydrometer and Thermometer for Brewing](http://tilthydrometer.com/products/brewometer) app and google sheet implementation. It's build to run on a Raspberry PI v3 which has Bluetooth beacon support. But could run on any platform that support php/python and has a Bluetooth device that supports beacons. 

### Prerequisite ###

* Python
* Webserver w/ php support
* Bluetooth with beacon support

Bluetooth on RPi tutorial will be added later. 

### Install ###

* Download code from bitbucket 
* Run ./setup.sh 
* All done! 

### Screenshoots ###
![TILTpi Screenshot](https://dev.n0ll.com/wordpress/wp-content/uploads/2017/01/tiltpi.png)