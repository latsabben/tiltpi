#!/usr/bin/env bash

set -ex

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   echo "Use sudo ./$0 - or change user to root"
   exit 1
fi

apt-get install -y lighttpd php5-cgi python-pip python-dev ipython bluetooth libbluetooth-dev

pip install pybluez

set +e
lighttpd-enable-mod fastcgi fastcgi-php
set -e

./setup.sh

WORKDIR=`pwd`
ln -sf $WORKDIR /var/www/html

/etc/init.d/lighttpd force-reload


