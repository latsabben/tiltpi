#!/usr/bin/env bash
set -e
echo -e "\e[96m  _______ _____ _   _______    _ "
echo -e " |__   __|_   _| | |__   __|  (_)"
echo -e "    | |    | | | |    | |_ __  _ "
echo -e "    | |    | | | |    | | '_ \| |"
echo -e "    | |   _| |_| |____| | |_) | |"
echo -e "    |_|  |_____|______|_| .__/|_|"
echo -e "                        | |      "
echo -e "                        |_|      \e[39m\n"

echo "### Tiltpi Setup script ##"
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   echo "Use sudo ./$0 - or change user to root"
   exit 1
fi

echo "Quickly setting up your structure and do a quick scan."
echo "If you have TILT please wake it up now! :)"
echo ""
read -p "Press any key to continue... " -n 1 -s
echo ""

echo -e "\n[*] Creating data structure"
mkdir -p DATA/batches
echo '{}' > DATA/tiltsdb
echo '{}' > DATA/batchdb
echo "[*] Setting permission "
chmod -R 755 *

echo "[*] Setting bluetooth stack permissions"
setcap cap_net_raw+eip $(eval readlink -f `which python`)
echo "[*] Running first time use of tiltblescan.py"
python tiltblescan.py

echo "[*] Checking for www-data user";
if [ -n $(getent passwd www-data) ]; then
	chown -R www-data:www-data *
        echo "[*] Adding crontab job for www-data to /etc/crontab"
	if grep -Fq "tiltblescan.py" /etc/crontab; then
    	echo [*] Crontab exisist...
	else
		WORKDIR=`pwd`
        printf "*/5 * * * *   www-data cd $WORKDIR && python tiltblescan.py\n" >> /etc/crontab
	fi
else
    echo "[E] - user www-data not found, please find apache/ngix/lighttpd"
    echo "      and run the following commands;"
    echo "      chown -R <www-user>:<www-group> *"
    echo ""
    echo "      and add the following line to /etc/crontab"
    echo "      */5 * * * *   <www-user> cd $WORKDIR && python tiltblescan.py"
fi

echo -e "\e[92m### Setup completed... ###\e[39m"
