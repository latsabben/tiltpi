<?php include("inc/header.php");?>



<h1>About</h1>
<p>Small project as a replacment for google sheet from <a href="http://tilthydrometer.com/">TILT&trade;</a>, I do not have smart phone that can be nearby and upload too google sheets. 
Raspberry pi 3 supports bluetooth beacons and is a perfect little device you can place near your TILT&trade; fermentation.</p>
<p>I'm no programmer, just a hobby hacker that likes to get things working. I've tried to make the source easy to read, and since there
is very little data (execept for the bluetooth beacon) everything is stored in files with json encoding. If you want to improve, add 
or have any suggestion let me know. Create pull/push request, or add tickets to the bitbucket account.<br>

<h4 class="text-center"><a href="https://bitbucket.org/lgpaulsen/tiltpi">Tilt&trade;(pi) @ bitbucket</a></h4>

</p>

<h3>Calculations</h3>
<p>During the testing, multiple deviation from was oberserved. The trend however is stabil. In order to compensate for the deviation found,
OG value is an average of the first 6 messuremnts, and the the SG is of the last 6 messurmenest. This will you get your a more "stabil" value.
To see if SG is stabil, I've added a meassurement too see how far back in time the SG has been stable. Since the SG variates easily 5 points between readings, and average for each hours to compensate for the spikes, and the if there are more then 2 point derivation it give you a time in days. (This is in testing, don't trust blindly...) </p>

<h3>License</h3>
<p>TILTpi is licensen under GNU GPLv3<br>
The componentes used are licenede under their respective licences. Everything is opensource, but HighCharts is only free for personal use. </p>

<?php include("inc/footer.php");?>
