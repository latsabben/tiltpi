import blescan
import sys
import time
import string
import json
import os.path
import bluetooth._bluetooth as bluez

dev_id = 0
timestamp = str(int(time.time()))
try:
    sock = bluez.hci_open_dev(dev_id)

except:
    print "error accessing bluetooth device..."
    sys.exit(1)

blescan.hci_le_set_scan_parameters(sock)
blescan.hci_enable_le_scan(sock)

returnedList = blescan.parse_events(sock, 10)
DATA = {}
for beacon in returnedList:
    arr = string.split(beacon, ",")
    UUID = arr[1]
    beacon = timestamp + "," + beacon
    DATA[UUID] = beacon


# Update / check for new TILTS
with open('DATA/tiltsdb') as tiltdb:
    tilts = json.load(tiltdb)
tilts_count = len(tilts)
for (uuid, data) in DATA.items():
    if uuid in tilts:
        print "I already have this TILT"
    else:
        tiltIDs = {
            'a495bb10c5b14b44b5121370f02d74de': 'RED',
            'a495bb20c5b14b44b5121370f02d74de': 'GREEN',
            'a495bb30c5b14b44b5121370f02d74de': 'BLACK',
            'a495bb40c5b14b44b5121370f02d74de': 'PURPLE',
            'a495bb50c5b14b44b5121370f02d74de': 'ORANGE',
            'a495bb60c5b14b44b5121370f02d74de': 'BLUE',
            'a495bb70c5b14b44b5121370f02d74de': 'YELLOW',
            'a495bb80c5b14b44b5121370f02d74de': 'PINK',
        }

        if uuid in tiltIDs:
            print("Adding: " + uuid)
            arr = string.split(data, ",")
            tilts[uuid] = {'UUID': uuid, 'MAC': arr[1],
                           "BATCH": "", "color": tiltIDs[uuid]}
        else:
            print("Beacon found, but not tils.. probably...")

if tilts_count != len(tilts):
    f = open('DATA/tiltsdb', 'w')
    f.write(json.dumps(tilts))
    f.close

# Add sleep to let filesystem free tiltsdb file
time.sleep(5)


# Writing data to disk
for (uuid, data) in DATA.items():
    # Check if a TILT or not
    if uuid in tilts:
        print uuid, data
        f = open('DATA/batches/' + uuid, 'a')
        f.write(data + '\n')
        f.close()
        # Write data to batchnummer
        try:
			batchName = tilts[uuid]['BATCH']
			print batchName
			if batchName != "" :
				print("Writing to batch: " + batchName)
				f = open('DATA/batches/' + batchName, 'a')
				f.write(data + '\n')
				f.close()
        except KeyError:
            # BATCH number not set for UUID, skipping
            pass
    else:
        print "Not a TILT: " + uuid, data
